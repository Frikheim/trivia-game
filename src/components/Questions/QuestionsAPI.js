export const QuestionsAPI = {
    //fetches question with the specified category number and difficulty
    fetchQuestions(difficulty,numberOfQuestions,category) {
        let url =  `https://opentdb.com/api.php?amount=${numberOfQuestions}&category=${category}&difficulty=${difficulty}`;
        return fetch(url)
        .then(response => response.json())
        //.then(data => data.results).

    },
    //fetches a list of the trivia categories
    fetchCategories() {
        return fetch("https://opentdb.com/api_category.php")
        .then(response => response.json())
        .then(data => data.trivia_categories)
    }
}