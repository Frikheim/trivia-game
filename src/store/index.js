import Vue from "vue";
import Vuex from "vuex";
import { QuestionsAPI} from "@/components/Questions/QuestionsAPI"

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        score: 0,
        difficulty: "easy",
        difficulties: ["easy","medium","hard"],
        numberOfQuestions: 1,
        category: 9,
        categories: [],
        //kanskje putte denne inne i questions?
        answers: [],
        answersContent: [],
        questions: [{"category":"General Knowledge","type":"boolean","difficulty":"easy","question":"You can legally drink alcohol while driving in Mississippi.","correct_answer":"True","incorrect_answers":["False"]}],
        currentQuestion: 0
    },
    mutations: {
        setScore: (state, payload) => {
            state.score = payload;
        },
        setDifficulty: (state, payload) => {
            state.difficulty = payload;
        },
        setNumberOfQuestions: (state, payload) => {
            state.numberOfQuestions = payload;
        },
        setCurrentQuestion: (state) => {
            //checks if currentQuestion is the last question
            if(state.currentQuestion === state.numberOfQuestions-1) {
                //if so, reset it
                state.currentQuestion = 0;
            }
            else {
               state.currentQuestion++; 
            }
        },
        setQuestions: (state, payload) => {
            state.questions = payload;
        },
        setCategory: (state, payload) => {
            state.category = payload;
        },
        //initialize array with false on each answer
        setAnswers: (state) => {
            let answersArray = []
            for(let i=0;i<state.numberOfQuestions;i++){
                answersArray.push(false);
            }
            state.answers = answersArray;
        },
        //sets the answer at a position to true
        setAnswer: (state,payload) =>{
            state.answers[payload] = true;
        },
        setCategories: (state,payload) => {
            state.categories = payload;
        },
        setAnswersContent: (state,payload) => {
            //adds the answer to the array
            state.answersContent.push(payload);
        },
        //resets the array
        resetAnswersContent: (state) => {
            state.answersContent = [];
        }
    },
    actions: {
        //method for fetching questions using QuestionsAPI
        async fetchQuestions({commit, state}) {
            try {
                const questions = 
                await QuestionsAPI.fetchQuestions(state.difficulty, state.numberOfQuestions, state.category);
                commit("setQuestions", questions.results);                
            } catch (error) {
                console.log(error);
            }
        },
        //method for fetching categories using QuestionsAPI
        async fetchCategories({commit}) {
            try {
                const categories =
                await QuestionsAPI.fetchCategories();
                commit("setCategories",categories);
            } catch (error) {
                console.log(error);
            }
        },
        //method for calculating the score from the array with true or false answers
        createScore({commit,state}) {
            let count = 0;
            for(let i=0;i<state.numberOfQuestions;i++) {
                if(state.answers[i]) {
                    count++;
                }
            }
            commit("setScore",count*10);
        }
    }    
})