import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "Start",
        component: () => import(/* webpackChunkName: "StartScreen" */"../components/Start/Start.vue")
    },
    {
        path: "/questions",
        name: "Questions",
        component: () => import(/* webpackChunkName: "Questions" */"../components/Questions/Questions.vue")
    },
    {
        path: "/result",
        name: "Result",
        component: () => import(/* webpackChunkName: "Result" */"../components/Result/Result.vue")
        },
];

export default new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});